pico-8 cartridge // http://www.pico-8.com
version 16
__lua__
function _init()

 local a = vec.new(2, 2)
 local b = vec.new(3, 3)
 print(b:unit():str())
 print(b:unit_approx():str())

end
-->8
--
-- shash
--
-- copyright (c) 2017 rxi
--
-- this library is free software; you can redistribute it and/or modify it
-- under the terms of the mit license. see license for details.
--

shash = { _version = "0.1.1" }
shash.__index = shash


function shash.new(cellsize)
  local self = setmetatable({}, shash)
  cellsize = cellsize or 64
  self.cellsize = cellsize
  self.tablepool = {}
  self.cells = {}
  self.entities = {}
  return self
end


local function coord_to_key(x, y)
  return x + y * 1e7
end


local function cell_position(cellsize, x, y)
  return flr(x / cellsize), flr(y / cellsize)
end


local function each_overlapping_cell(self, e, fn, ...)
  local cellsize = self.cellsize
  local sx, sy = cell_position(cellsize, e[1], e[2])
  local ex, ey = cell_position(cellsize, e[3], e[4])
  for y = sy, ey do
    for x = sx, ex do
      local idx = coord_to_key(x, y)
      fn(self, idx, ...)
    end
  end
end


local function add_entity_to_cell(self, idx, e)
  if not self.cells[idx] then
    self.cells[idx] = { e }
  else
    table.insert(self.cells[idx], e)
  end
end


local function remove_entity_from_cell(self, idx, e)
  local t = self.cells[idx]
  local n = #t
  -- only one entity? remove entity from cell and remove cell
  if n == 1 then
    self.cells[idx] = nil
    return
  end
  -- find and swap-remove entity
  for i, v in ipairs(t) do
    if v == e then
      t[i] = t[n]
      t[n] = nil
      return
    end
  end
end


function shash:add(obj, x, y, w, h)
  -- create entity. the table is used as an array as this offers a noticable
  -- performance increase on luajit; the indices are as follows:
  -- [1] = left, [2] = top, [3] = right, [4] = bottom, [5] = object
  local e = { x, y, x + w, y + h, obj }
  -- add to main entities table
  self.entities[obj] = e
  -- add to cells
  each_overlapping_cell(self, e, add_entity_to_cell, e)
end


function shash:remove(obj)
  -- get entity of obj
  local e = self.entities[obj]
  -- remove from main entities table
  self.entities[obj] = nil
  -- remove from cells
  each_overlapping_cell(self, e, remove_entity_from_cell, e)
end


function shash:update(obj, x, y, w, h)
  -- get entity from obj
  local e = self.entities[obj]
  -- no width/height specified? get width/height from existing bounding box
  w = w or e[3] - e[1]
  h = h or e[4] - e[2]
  -- check the entity has actually changed cell-position, if it hasn't we don't
  -- need to touch the cells at all
  local cellsize = self.cellsize
  local ax1, ay1 = cell_position(cellsize, e[1], e[2])
  local ax2, ay2 = cell_position(cellsize, e[3], e[4])
  local bx1, by1 = cell_position(cellsize, x, y)
  local bx2, by2 = cell_position(cellsize, x + w, y + h)
  local dirty = ax1 ~= bx1 or ay1 ~= by1 or ax2 ~= bx2 or ay2 ~= by2
  -- remove from old cells
  if dirty then
    each_overlapping_cell(self, e, remove_entity_from_cell, e)
  end
  -- update entity
  e[1], e[2], e[3], e[4] = x, y, x + w, y + h
  -- add to new cells
  if dirty then
    each_overlapping_cell(self, e, add_entity_to_cell, e)
  end
end


function shash:clear()
  -- clear all cells and entities
  for k in pairs(self.cells) do
    self.cells[k] = nil
  end
  for k in pairs(self.entities) do
    self.entities[k] = nil
  end
end


local function overlaps(e1, e2)
  return e1[3] > e2[1] and e1[1] < e2[3] and e1[4] > e2[2] and e1[2] < e2[4]
end


local function each_overlapping_in_cell(self, idx, e, set, fn, ...)
  local t = self.cells[idx]
  if not t then
    return
  end
  for i, v in ipairs(t) do
    if e ~= v and overlaps(e, v) and not set[v] then
      fn(v[5], ...)
      set[v] = true
    end
  end
end


local function each_overlapping_entity(self, e, fn, ...)
  -- init set for keeping track of which entities have already been handled
  local set = table.remove(self.tablepool) or {}
  -- do overlap checks
  each_overlapping_cell(self, e, each_overlapping_in_cell, e, set, fn, ...)
  -- clear set and return to pool
  for v in pairs(set) do
    set[v] = nil
  end
  table.insert(self.tablepool, set)
end


function shash:each(x, y, w, h, fn, ...)
  local e = self.entities[x]
  if e then
    -- got object, use its entity
    each_overlapping_entity(self, e, y, w, h, fn, ...)
  else
    -- got bounding box, make temporary entity
    each_overlapping_entity(self, { x, y, x + w, y + h }, fn, ...)
  end
end


function shash:info(opt, ...)
  if opt == "cells" or opt == "entities" then
    local n = 0
    for k in pairs(self[opt]) do
      n = n + 1
    end
    return n
  end
  if opt == "cell" then
    local t = self.cells[ coord_to_key(...) ]
    return t and #t or 0
  end
  error( string.format("invalid opt '%s'", opt) )
end

-- end shash
-->8
-- pico8-missing-builtins v0.2.0
-- https://github.com/adamscott/pico8-missing-builtins

-- rawget depends on getmetatable
function rawget (tbl, index)
  assert(type(tbl) == 'table', "bad argument #1 to 'rawget' "
    .."(table expected, got "..type(tbl)..")")
  local ti = tbl.__index
  local mt = getmetatable(tbl)
  local value = nil
  tbl.__index = tbl
  __setmetatable(tbl, nil)
  value = tbl[index]
  tbl.__index = ti
  __setmetatable(tbl, mt)
  return value
end

function unpack (arr, i, j)
  local n = {}
  local k = 0
  local initial_i = i
  j = j or #arr
  for i = i or 1, j do
    k = k + 1
    n[k] = arr[i]
  end
  local l = k
  local function create_arg(l, ...)
    if l == 0 then
      return ...
    else
      return create_arg(l - 1, n[l], ...)
    end
  end
  return create_arg(l)
end

function ipairs (a)
  local function iter(a, i)
    i = i + 1
    local v = a[i]
    if v then
      return i, v
    end
  end
  return iter, a, 0
end

table = {}
table.pack = function (...) return {...} end
table.unpack = unpack

function table.insert (list, pos, value)
  assert(type(list) == 'table', "bad argument #1 to 'insert' "
    .."(table expected, got "..type(list)..")")
  if pos and not value then
    value = pos
    pos = #list + 1
  else
    assert(type(pos) == 'number', "bad argument #2 to 'insert' "
      .."(number expected, got "..type(pos)..")")
  end
  if pos <= #list then
    for i = #list, pos, -1 do
      list[i + 1] = list[i]
    end
  end
  list[pos] = value
end

function table.remove(list, pos)
  assert(type(list) == 'table', "bad argument #1 to 'remove' "
    .."(table expected, got "..type(list)..")")
  if not pos then
    pos = #list
  else
    assert(type(pos) == 'number', "bad argument #2 to 'remove' "
      .."(number expected, got "..type(tbl)..")")
  end
  for i = pos, #list do
    list[i] = list[i + 1]
  end
end

function table.sort (arr, comp)
  if not comp then
    comp = function (a, b)
      return a < b
    end
  end
  local function partition (a, lo, hi)
      pivot = a[hi]
      i = lo - 1
      for j = lo, hi - 1 do
        if comp(a[j], pivot) then
          i = i + 1
          a[i], a[j] = a[j], a[i]
        end
      end
      a[i + 1], a[hi] = a[hi], a[i + 1]
      return i + 1
    end
  local function quicksort (a, lo, hi)
    if lo < hi then
      p = partition(a, lo, hi)
      quicksort(a, lo, p - 1)
      return quicksort(a, p + 1, hi)
    end
  end
  return quicksort(arr, 1, #arr)
end

-- end pico8-missing-builtins v0.2.0
-->8
-- 2d vector math
--
-- translated from
-- https://peeke.nl/simulating-blobs-of-fluid

vec = {}
vec.__index = vec

function vec.new(x, y)
 local self = setmetatable({}, vec)
 self.x = x
 self.y = y

 return self
end

function vec.__add(a, b)
	return vec.new(
		a.x + b.x,
		a.y + b.y
	)
end

function vec.__sub(a, b)
	return vec.new(
		a.x - b.x,
		a.y - b.y
	)
end

-- vector * number: scalar product
-- vector * vector: dot product
function vec.__mul(a, b)
 if type(a) == "number" then
  return vec.new(
   b.x * a, b.y * a
  )
 elseif type(b) == "number" then
  return vec.new(
   a.x * b,
   a.y * b
  )
 end

 return vec.new(
  a.x * b.x,
  a.y * b.y
 )
end

function vec:length_squared()
 return self.x * self.x
  + self.y * self.y
end

function vec:length()
 return sqrt(self:length_squared())
end

function vec:unit()
 local length = self:length()
 if length == 0 then
  return vec.new(0, 0)
 end
 
 return self * (1 / length)
end

function vec:unit_approx()
 if self.x == 0 and self.y == 0 then
  return vec.new(0, 0)
 end

 local ax = abs(self.x)
 local ay = abs(self.y)
 
 local ratio = 1 / max(ax, ay)
 ratio *= (1.29289 - (ax + ay) * ratio * 0.29289)

	return self * ratio
end

function vec:str()
 return "(" .. self.x .. ", " .. self.y .. ")"
end

-- end 2d vector math
